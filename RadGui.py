from PyQt5.QtWidgets import QMainWindow, QApplication,QWidget
from PyQt5 import uic
from PyQt5.QtGui import *
from PyQt5.QtCore import Qt,pyqtSlot
from PyQt5 import QtCore
import numpy as np
import sys
import os
from collections import deque
import datetime
import graphyte

#local imports
from record_star_data import loop_chips
from read_rpi import rpi
from Instek import Instek
Ui_MainWindow, QtBaseClass = uic.loadUiType("RadGui.ui")
from tec import tec
#from sht85 import sht85
import threading
import time
from threading import Thread
from Initialize import chip_dict
home="/home/grosin/Radiation"
try:
    ThreeVolts=Instek("/dev/ttyUSB0")
    OneFiveVolts=Instek("/dev/ttyUSB2")
except:
    print("did not detect insteks")
def write_csv(values,filename="eviroment_data.csv"):
    if not os.path.isfile(filename):
        with open(filename,"a") as csv:
            csv.write("Time,")
            csv.write("TEC Temperature,")
            csv.write("humidity,")
            csv.write("Air Temperature,")
            csv.write("3V3 voltage,")
            csv.write("3V3 current,")
            csv.write("1V5 digital voltage,")
            csv.write("1V5 digital current,")
            csv.write("1V5 analog voltage,")
            csv.write("1V5 analog current")
            csv.write("\n")
    with open(filename,"a") as csv:
        for value in values:
            csv.write(str(value))
            if value != values[-1]:
                csv.write(",")
        csv.write("\n")
        
def check_stability(temperature_deque):
    tolerance=1.5
    return np.std(temperature_deque)<tolerance

graphyte.init("127.0.0.1")
class MyApp(QMainWindow):
    def __init__(self):
        super(MyApp, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        #self.ui.TemperatureSlider11.setTickPosition(2)
        #self.ui.TemperatureSlider11.setTickInterval(5)

        #temperature sliders
        self.ui.TemperatureSlider11.setMinimum(-15)
        self.ui.TemperatureSlider11.setMaximum(25)
        self.ui.TemperatureSlider11.setValue(20)
        self.ui.TemperatureDisplay11.setText(str(self.ui.TemperatureSlider11.value()))
        self.ui.TemperatureSlider11.valueChanged.connect(self.TempSlider)
        self.ui.TemperatureSlider11.sliderReleased.connect(self.SetTempFromSlider)

        self.ui.TemperatureSlider12.setMinimum(-15)
        self.ui.TemperatureSlider12.setMaximum(25)
        self.ui.TemperatureSlider12.setValue(20)
        self.ui.TemperatureDisplay12.setText(str(self.ui.TemperatureSlider12.value()))
        self.ui.TemperatureSlider12.valueChanged.connect(self.TempSlider)
        self.ui.TemperatureSlider12.sliderReleased.connect(self.SetTempFromSlider)

        self.ui.TemperatureSlider13.setMinimum(-15)
        self.ui.TemperatureSlider13.setMaximum(25)
        self.ui.TemperatureSlider13.setValue(20)
        self.ui.TemperatureDisplay13.setText(str(self.ui.TemperatureSlider13.value()))
        self.ui.TemperatureSlider13.valueChanged.connect(self.TempSlider)
        self.ui.TemperatureSlider13.sliderReleased.connect(self.SetTempFromSlider)

        self.ui.TemperatureSlider14.setMinimum(-15)
        self.ui.TemperatureSlider14.setMaximum(25)
        self.ui.TemperatureSlider14.setValue(20)
        self.ui.TemperatureDisplay14.setText(str(self.ui.TemperatureSlider14.value()))
        self.ui.TemperatureSlider14.valueChanged.connect(self.TempSlider)
        self.ui.TemperatureSlider14.sliderReleased.connect(self.SetTempFromSlider)

        self.ui.sensorButton.clicked.connect(self.start_monitoring)
        self.chip11_chiller=tec(chip_dict["Chip11"].tec,230400,1)
        self.chip12_chiller=tec(chip_dict["Chip12"].tec,230400,2)
        self.chip13_chiller=tec(chip_dict["Chip13"].tec,230400,3)
        self.chip14_chiller=tec(chip_dict["Chip14"].tec,230400,4)

        self.ui.tec11Output.clicked.connect(lambda : self.tec_output(self.ui.tec11Output,self.chip11_chiller))
        self.ui.tec12Output.clicked.connect(lambda : self.tec_output(self.ui.tec12Output,self.chip12_chiller))
        self.ui.tec13Output.clicked.connect(lambda : self.tec_output(self.ui.tec13Output,self.chip13_chiller))
        self.ui.tec14Output.clicked.connect(lambda : self.tec_output(self.ui.tec14Output,self.chip14_chiller))

        
        try:
            process = Thread(target=self.chip11_chiller.default_setting)
            process.start()
        except:
            pass
        try:
            process = Thread(target=self.chip12_chiller.default_setting)
            process.start()
        except:
            pass
        try:
            process = Thread(target=self.chip13_chiller.default_setting)
            process.start()
        except:
            pass
        try:
            process = Thread(target=self.chip14_chiller.default_setting)
            process.start()
        except:
            pass
        
        self.chip11_temp=20
        self.chip12_temp=20
        self.chip13_temp=20
        self.chip14_temp=20

        self.chip11_humid=19
        self.chip12_humid=20
        self.chip13_humid=21
        self.chip14_humid=22

        self.ui.humidity11.setValue(self.chip11_humid)
        self.ui.humidity12.setValue(self.chip12_humid)
        self.ui.humidity13.setValue(self.chip13_humid)
        self.ui.humidity14.setValue(self.chip14_humid)

        self.pill2kill = threading.Event()
        self.monitor=SensorMonitor("Sensor Thread 1",self.pill2kill,[self.chip11_chiller,self.chip12_chiller,self.chip13_chiller,self.chip14_chiller])
        self.monitor.humid_sig.connect(self.UpdateHumidity)
        self.monitor.temp_sig.connect(self.UpdateTemperature)
        self.monitor.power_sig.connect(self.UpdateCurrent)

        self.ABCKiller=threading.Event()
        self.ABCMonitor=ABCStarMonitor("ABC Monitor thread 1",self.ABCKiller)
        self.ui.ABCStarButton.clicked.connect(self.ABCStarMonitor)
        self.ui.PanicButton.setStyleSheet("background-color: red;font: bold 20px")
        self.ui.PanicButton.clicked.connect(self.panic)

        self.ui.fmcinstekon.clicked.connect(self.TurnFmcOn)
        self.ui.starinstekon.clicked.connect(self.TurnStarOn)

        self.ui.fmc_channel1.returnPressed.connect(lambda: self.SetVoltage(ThreeVolts,float(self.ui.fmc_channel1.text()),1))
        self.ui.fmc_channel2.returnPressed.connect(lambda: self.SetVoltage(ThreeVolts,float(self.ui.fmc_channel2.text()),2))
        self.ui.fmc_channel3.returnPressed.connect(lambda: self.SetVoltage(ThreeVolts,float(self.ui.fmc_channel3.text()),3))

        self.ui.star_channel1.returnPressed.connect(lambda: self.SetVoltage(OneFiveVolts,float(self.ui.star_channel1.text()),1))
        self.ui.star_channel2.returnPressed.connect(lambda: self.SetVoltage(OneFiveVolts,float(self.ui.star_channel2.text()),2))
        self.ui.star_channel3.returnPressed.connect(lambda: self.SetVoltage(OneFiveVolts,float(self.ui.star_channel3.text()),3))

        self.ui.currents.clicked.connect(self.updateCurrents)
        
    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)

        y_pos=55
        x_pos=170
        spacing=100
        
        temp_11=self.chip11_temp

        grad1 = QLinearGradient(x_pos,y_pos,x_pos+25,y_pos+165)
        grad1.setColorAt(1,QColor(255,0,0))
        grad1.setColorAt((25.0001-temp_11)/40.1+0.001,QColor(170,0,80))
        grad1.setColorAt(0,QColor(0,0,255))
        qp.setBrush(grad1)
        qp.drawRect(x_pos,y_pos,25,165)
        qp.end()

        qp2 = QPainter()
        qp2.begin(self)
        temp_12=self.chip12_temp
        grad2 = QLinearGradient(x_pos+spacing,y_pos,x_pos+spacing+25,y_pos+165)
        grad2.setColorAt(1,QColor(255,0,0))
        grad2.setColorAt((25.0001-temp_12)/40.1+0.001,QColor(170,0,80))
        grad2.setColorAt(0,QColor(0,0,255))
        qp2.setBrush(grad2)
        qp2.drawRect(x_pos+spacing,y_pos,25,165)
        qp2.end()

        qp3 = QPainter()
        qp3.begin(self)
        temp_13=self.chip13_temp
        grad3 = QLinearGradient(x_pos+spacing*2,y_pos,x_pos+spacing*2+25,y_pos+165)
        grad3.setColorAt(1,QColor(255,0,0))
        grad3.setColorAt((25.0001-temp_13)/40.1+0.001,QColor(170,0,80))
        grad3.setColorAt(0,QColor(0,0,255))
        qp3.setBrush(grad3)
        qp3.drawRect(x_pos+2*spacing,y_pos,25,165)
        qp3.end()

        
        qp4 = QPainter()
        qp4.begin(self)
        temp_14=self.chip14_temp
        grad4 = QLinearGradient(x_pos+spacing*3,y_pos,x_pos+spacing*3+25,y_pos+165)
        grad4.setColorAt(1,QColor(255,0,0))
        grad4.setColorAt((25.001-temp_14)/40.1+.001,QColor(170,0,80))
        grad4.setColorAt(0,QColor(0,0,255))
        qp4.setBrush(grad4)
        qp4.drawRect(x_pos+spacing*3,y_pos,25,165)
        
        qp4.end()

    def start_monitoring(self):
        if not self.pill2kill.is_set():
            self.pill2kill.set()
            self.monitor.start()
        else:
            self.pill2kill.clear()

    def TempSlider(self):
        self.ui.TemperatureDisplay11.setText(str(self.ui.TemperatureSlider11.value()))
        self.ui.TemperatureDisplay12.setText(str(self.ui.TemperatureSlider12.value()))
        self.ui.TemperatureDisplay13.setText(str(self.ui.TemperatureSlider13.value()))
        self.ui.TemperatureDisplay14.setText(str(self.ui.TemperatureSlider14.value()))
        self.update()

    def SetTempFromSlider(self):
        try:
            self.chip11_chiller.set_temp(int(self.ui.TemperatureSlider11.value()))
        except:
            print("didnt set chip 11 temp")
        try:
            self.chip12_chiller.set_temp(int(self.ui.TemperatureSlider12.value()))
        except:
            print("didnt set chip 12 temp")
        try:
            self.chip13_chiller.set_temp(int(self.ui.TemperatureSlider13.value()))
        except:
            print("didnt set chip 13 temp")
        try:
            self.chip14_chiller.set_temp(int(self.ui.TemperatureSlider14.value()))
        except:
            print("didnt set chip 14 temp")

        
    @pyqtSlot(int,float)
    def UpdateHumidity(self,chip_id,value):
        print("updating humidity")
        print(chip_id,value)
        if chip_id == 1:
            self.ui.humidity11.setValue(value)
        if chip_id == 2:
            self.ui.humidity12.setValue(value)
        if chip_id == 3:
            self.ui.humidity13.setValue(value)
        if chip_id == 4:
            self.ui.humidity14.setValue(value)
        self.update()
    @pyqtSlot(int,float)
    def UpdateTemperature(self,chip_id,value):
        print("updating temperature")
        print(chip_id,value)
        if chip_id == 1:
            self.chip11_temp=value
            self.ui.Chip11TecTemp.setText(str(round(value,1)))
        if chip_id == 2:
            self.chip12_temp=value
            self.ui.Chip12TecTemp.setText(str(round(value,1)))
        if chip_id == 3:
            self.chip13_temp=value
            self.ui.Chip13TecTemp.setText(str(round(value,1)))
        if chip_id == 4:
            self.chip14_temp=value
            self.ui.Chip14TecTemp.setText(str(round(value,1)))
        self.update()


    @pyqtSlot(str,float)
    def UpdateCurrent(self,source,current):
        if source.lower() == 'digital':
            self.ui.Digital.setText(str(current))
        elif source.lower() == 'analog':
            self.ui.Analog.setText(str(current))
        else:
            source.ui.ThreeVoltCurrent.setText(str(current))

    def ABCStarMonitor(self):
        if not self.ABCKiller.is_set():
            self.ABCKiller.set()
            self.ABCMonitor.start()
        else:
            self.ABCKiller.clear()

    def panic(self):
        self.chip11_chiller.set_temp(20)
        self.chip12_chiller.set_temp(20)
        self.chip13_chiller.set_temp(20)
        self.chip14_chiller.set_temp(20)
        OneFiveVolts.TurnOff()
        time.sleep(0.1)
        ThreeVolts.TurnOff()

    def tec_output(self,button,chiller):
        try:
            if button.isChecked():
                chiller.turn_on()
            else:
                chiller.turn_off()
        except Exception as e:
            print(e)
            print("did not set output")
    def TurnFmcOn(self):
        if( self.ui.fmcinstekon.isChecked()):
            ThreeVolts.TurnOn()
        else:
            ThreeVolts.TurnOff()
        
    def TurnStarOn(self):
        if( self.ui.starinstekon.isChecked()):
            OneFiveVolts.TurnOn()
        else:
            OneFiveVolts.TurnOff()
        
    def SetVoltage(self,ps,value,channel):
        ps.SetVoltage(value,channel)

    def updateCurrents(self):
        try:
            print(ThreeVolts.GetActualCurrent(1))
            self.ui.ThreeVoltCurrent.setText(ThreeVolts.GetActualCurrent(1).decode())
            self.ui.Digital.setText(OneFiveVolts.GetActualCurrent(1).decode())
            self.ui.Analog.setText(OneFiveVolts.GetActualCurrent(2).decode())
        except Exception as e:
            print(e)
class SensorMonitor(QtCore.QThread):
    humid_sig = QtCore.pyqtSignal(int,float)
    temp_sig= QtCore.pyqtSignal(int,float)
    power_sig= QtCore.pyqtSignal(str,float)

    error_sig=QtCore.pyqtSignal(str)
    def __init__(self,threadID,stop_event,tec_list):
        super(QtCore.QThread, self).__init__()
        self.threadID = threadID
        self.temperature_deque=deque()
        self.tec_list=tec_list
        self.stop_event=stop_event
        try:
            self.pi=rpi("/dev/ttyUSB10")
        except:
            print("did not detect rpi")
        graphyte.init("127.0.0.1")
    def run(self):
        print("Starting " + str(self.threadID))
        self.monitor()

    def monitor(self):
        while self.stop_event.is_set():
            time.sleep(1)
            chip_values=[[str(datetime.datetime.now())],[str(datetime.datetime.now())],[str(datetime.datetime.now())],[str(datetime.datetime.now())]]
            for tec in self.tec_list:
                if tec:
                    try:
                        tec_temp=tec.get_temp()
                        print(tec_temp)
                        print(tec.chip_id)
                        graphyte.send("chip1"+str(tec.chip_id)+".tec_temperature",tec_temp)
                        chip_values[tec.chip_id-1].append(tec_temp)
                        if(tec_temp > 25):
                            print("high temperature, not recording")
                        else:
                            self.temp_sig.emit(tec.chip_id,tec_temp)
                    except Exception as e:
                        print(e)
                        print("problem with TEC")

            try:
                sht_data=self.pi.get_data()
            except:
                sht_data=''
            while sht_data['chip']:
                chip_id=sht_data['chip']
                sht_temp=sht_data['Temperature']
                sht_hum=sht_data['Humidity']
                chip_values[int(chip_id[-1])-1].append(sht_hum)
                chip_values[int(chip_id[-1])-1].append(sht_temp)

                graphyte.send("chip1"+str(chip_id[-1])+".humidity",sht_hum)
                graphyte.send("chip1"+str(chip_id[-1])+".air_temperature",sht_temp)

                print(sht_data)
                self.humid_sig.emit(int(chip_id[-1]),float(sht_hum))
                time.sleep(0.1)
                if sht_hum > 30:
                    self.tec_list[int(chip_id[-1])-1].set_temp(20)
                try:
                    sht_data=self.pi.get_data()
                except:
                    sht_data=''

            for i,values in enumerate(chip_values):
                try:
                    values.append(ThreeVolts.GetActualVoltage())
                    values.append(ThreeVolts.GetActualCurrent())
                    values.append(OneFiveVolts.GetActualVoltage(1))
                    values.append(OneFiveVolts.GetActualCurrent(1))
                    values.append(OneFiveVolts.GetActualVoltage(2))
                    values.append(OneFiveVolts.GetActualCurrent(2))
                except:
                    pass
                write_csv(values,home+"/results/enviromental_data_chip1"+str(i+1)+".csv")
                write_csv(values,"/home/grosin/google-drive/enviromental_data_chip1"+str(i+1)+".csv")

        print("stopped")


class ABCStarMonitor(QtCore.QThread):
    def __init__(self,threadID,stop_event):
        super(QtCore.QThread, self).__init__()
        self.threadID = threadID
        self.stop_event=stop_event

    def run(self):
        print("starting"+self.threadID)
        while self.stop_event.is_set():
            print("taking fmc data")
            try:
                loop_chips("/home/grosin/Radiation/results/")
            except Exception as e:
                print(e)
                with open(home+"/log/error_log.txt","a") as errorLog:
                    errorLog.write(str(datetime.datetime.now))
                    errorLog.write(",")
                    errorLog.write("exception from FMC monitoring")
                    errorLog.write(e)
                    errorLog.write("\n")
                    
            time.sleep(30)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
    
