import time
import serial
import re
import numpy as np
import serial.tools.list_ports
def get_checksum(command):
    chksum=0
    for char in command:
        chksum+=ord(char)
    chksum=chksum% 256
    return hex(chksum)[2:]


def twos_comp(val, bits):
    mask = 2**(bits - 1) - 1
    return -(val& mask) + (val & ~mask)

def hex_temp(value):
    value=value*100
    if value < 0:
        return hex(twos_comp(value,16)).split('x')[1]
    else:
        return hex(value).split('x')[1]

def pad_hex(value):
    if len(value)>=4:
        return value
    return pad_hex('0'+value)


def build_command(temp):
    command=['*','1','c']
    temp_value=pad_hex(hex_temp(temp))
    for char in temp_value:
        command.append(char)

    chksum=get_checksum(command[1:])
    for char in chksum:
        command.append(char)
    command.append("\r")
    return command

def build_id(pin):
    command=["*","2","1"]
    pin_value=pad_hex(hex_temp(pin))
    for char in pin_value:
        command.append(char)

    chksum=get_checksum(command[1:])
    for char in chksum:
        command.append(char)
    command.append("\r")
    return command

def parse_hex(hex_data,i=0):
    hexvaluearr=re.findall(r"[0-9a-fA-F]+",hex_data)
    print(hexvaluearr)
    hexvalue=hexvaluearr[i]
    hexvalue=hexvalue[:-2]
    temp=int(hexvalue,16)
    if(temp > 500):
        return float(twos_comp(int(hexvalue,16),16))/100.0*-1
    return float(temp)/100.0

class tec:
    def __init__(self,port,baud=230400,chip_id=1):
        try:
            self.device=serial.Serial(port,baud,timeout=5)
        except serial.serialutil.SerialException as e:
            print(e)
            print("could not find tec")
        self.stable=True
        self.chip_id=chip_id
    def set_temp(self,temp):
        try:
            command=build_command(temp)
            for char in command:
                self.device.write(char.encode())
                time.sleep(0.004)
            self.stable=False
        except Exception as e:
            print(e)
            print("did not set temp")

    def turn_on(self):
        try:
            command=['*','3','0','0','0','0','a','5','4','\r']
            for char in command:
                self.device.write(char.encode())
                time.sleep(0.004)
            self.stable=False
        except Exception as e:
            print(e)
            print("did not turn on ")


    def turn_off(self):
        try:
            command=['*','3','0','0','0','0','0','2','3','\r']
            for char in command:
                self.device.write(char.encode())
                time.sleep(0.004)
            self.stable=False
        except Exception as e:
            print(e)
            print("did not turn off ")

    def get_temp(self):
        try:
            command=['*','0','1','0','0','0','0','2','1','\r']
            for char in command:
                self.device.write(char.encode())
                time.sleep(0.004)
            data=self.device.readline().decode()
            print(data)
            hexvaluearr=re.findall(r"[0-9a-fA-F]+",data)
            #hexvalue=hexvaluearr[0] if hexvaluearr[0]!='0' else hexvaluearr[1]
            hexvalue=hexvaluearr[-1]
            hexvalue=hexvalue[:-2]
            temp=int(hexvalue,16)
            if(temp > 500):
                return float(twos_comp(int(hexvalue,16),16))/100.0*-1
            return float(temp)/100.0
        except Exception as e:
            print(e)
            print("can't get data")
            return 20

    def set_id(self,pin):
        try:
            command=build_id(pin)
            for char in command:
                self.device.write(char.encode())
                time.sleep(0.004)
        except Exception as e:
            print(e)
            print("did not set id")
        
    def get_id(self):
        try:
            command=['*','5','5','0','0','0','0']
            chksum=get_checksum(command[-1])
            for char in chksum:
                command.append(char)
            command.append("\r")

            for char in command:
                self.device.write(char.encode())
                time.sleep(0.004)
            data=self.device.readline().decode()
            hexvaluearr=re.findall(r"[0-9a-fA-F]+",data)
            #hexvalue=hexvaluearr[0] if hexvaluearr[0]!='0' else hexvaluearr[1]
            hexvalue=hexvaluearr[-1]
            hexvalue=hexvalue[:-2]
            pin=int(hexvalue,16)
            return pin/100
        except Exception as e:
            print(e)
            print("can't get pin number")
            return 20        
    def default_setting(self):
        try:
            d_command=['*','1','f','0','0','0','0','5','7','\r']
            i_command=['*','1','e','0','0','6','4','6','0','\r']
            band_command=['*','1','d','0','0','6','4','5','f','\r']
            eeprom =["*","3",'1','0','0','0','0','2','5','\r']
            for char in d_command:
                self.device.write(char.encode())
                time.sleep(0.004)
            time.sleep(1)
            for char in i_command:
                self.device.write(char.encode())
                time.sleep(0.004)
            time.sleep(1)     
            for char in band_command:
                self.device.write(char.encode())
                time.sleep(0.004)
            time.sleep(1)
            for char in eeprom:
                self.device.write(char.encode())
                time.sleep(0.004)
            time.sleep(1)
        except:
            print("did not set defaults")

def list_availble_tec():
    ports=list(serial.tools.list_ports.comports())
    tec_dict={}
    for port in port:
        try:
            test_tec=tect(port)
            tec_id=test_tec.get_id()
            print("chip tec:"+str(tec_id)+" port:"+str(port))
            tec_dict["chip"+str(tec_id)]=str(port)
        except:
            pass
    return tec_dict

if __name__=="__main__":
    list_availble_tec()
