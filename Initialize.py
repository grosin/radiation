try:
    import configparser
except ImportError:
    import ConfigParser as configparser
from tec import list_availble_tec

class ABCStar:
    def __init__(self,chipid,tec,SHT_bus,dose,color):
        self.chipid=chipid
        self.tec=tec
        self.bus=SHT_bus
        self.dose=dose
        self.color=color

    def __str__(self):
        return "Chip:"+str(self.chipid)+"\nTec Port:"+str(self.tec)+"\ncolor:"+str(self.color)+"\ndose:"+str(self.dose)


def get_configs():
    parser = configparser.ConfigParser()
    parser.read("configurations.ini")
    sections=parser.sections()
    chip_dict={}
    for section in sections:
        chip_dict[str(section)]=ABCStar(section,parser[section]['TEC'],
                                        parser[section]['SHT_bus'],
                                        parser[section]['dose'],
                                        parser[section]['color'])
    try:
        tec_dict=list_availble_tec()
        for chip,port in tec_dict.items():
            chip_dict[chip].tec=port
    except:
        pass
    
    return chip_dict

chip_dict=get_configs()
